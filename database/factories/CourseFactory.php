<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'text' => $faker->paragraph(1),
        'name' => $faker->word,
        'category' => $faker->word,
        'price' => $faker->randomElement([100,250,500,750,1000]),
    ];
});
