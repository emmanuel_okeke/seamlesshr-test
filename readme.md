## API Information

METHOD | DESCRIPTION | ENDPOINTS
-------|-------------|-----------
POST   | User registration | `/api/auth/register`
POST   | User log in | `/api/auth/loging`
POST   | User logout | `/api/auth/logout`
POST    | Get user details | `/api/auth/me`
POST    | Refresh user token | `/api/auth/refresh`
GET    | Get all courses | `/api/all`
POST    | Course Registeration | `/api/courses/register`
GET   | Creat 50 course records   | `/api/factory/create`
GET   | Get all courses as excel  | `/api/export`

### All routes are been gaurded. 