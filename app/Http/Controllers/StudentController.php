<?php

namespace App\Http\Controllers;

use App\Course;
use App\student;
use Validator;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
  
    public function courseRegistration(Request $request)
    {
        $credentials = $request->only('user_id', 'course_id');
        
        $rules = [
            'user_id' => 'required',
            'course_id' => 'required'
        ];
        $validator = Validator::make($credentials, $rules);

        $user_id = $request->user_id;
        $course_id = $request->course_id;


        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        
        $user = Student::create(['user_id' => $user_id, 'course_id' => $course_id]);
        
        return response()->json(['success'=> true, 'message'=> 'Registeration successful!']);
    }
}
