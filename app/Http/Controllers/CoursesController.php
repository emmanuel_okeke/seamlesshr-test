<?php

namespace App\Http\Controllers;

use App\Course;
use App\Student;
use App\Jobs\CreateFiftyCourses;
use Illuminate\Http\Request;
use App\Exports\CoursesExport;
use Maatwebsite\Excel\Facades\Excel;

class CoursesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // List all courses
    public function allCourses()
    {
        $courses = Course::all();

        $data = [];

        foreach ($courses as $course) {
            $results = [
                'text' => $course->text,
                'name' => $course->name,
                'category' => $course->category,
                'price' => $course->price,
                'create_at'=> $course->created_at,
            ];

            $data[] = $results;
        }
        return $data;
    }

    // Add 50 courses using factory
    public function addCourses() 
    {
        $queue = CreateFiftyCourses::dispatch();
    }

    // Export all course in as excel
    public function export() 
    {
        return Excel::download(new CoursesExport, 'courses.xlsx');
    }
}
